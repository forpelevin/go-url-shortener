package service

import (
	"context"
	"math/rand"
	"time"

	"gitlab.com/forpelevin/go-url-shortener/internal/pb"
	"gitlab.com/forpelevin/go-url-shortener/internal/storage"
	"gitlab.com/forpelevin/go-url-shortener/internal/validator"
)

const (
	alphabet     = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	alphabetLen  = len(alphabet)
	shortPathLen = 5
)

// Shortener is the interface that wraps the methods for shortening or resolving URLs.
//
// Shorten accepts an URL as argument, shorts it, and returns the short version.
// Resolve accepts a shortened URL as argument, and returns a corresponding original URL.
//
// Implementations must solve which shorting algorithm to use and where store a shortened URL for future resolving.
type Shortener interface {
	Shorten(string) string
	Resolve(string) string
}

// ShortenerService is the struct for shortening and resolving URLs.
// It uses a LinkStorage as a storage for shortened links.
// Feel free to make your own implementation of the storage.
type ShortenerService struct {
	linkStorage storage.LinkStorage
}

// Shorten accepts an URL as argument, shorts it, and returns the short version.
// It generates a random 5-length string as short path using default alphabet,
// appends it to the given URL's host, and stores the relation in the LinkStorage.
// Default short path generator is protected from collisions.
func (service *ShortenerService) Shorten(ctx context.Context, r *pb.ShortenRequest) (*pb.ShortenResponse, error) {
	// If there is an invalid url without host then we won't proceed the given string.
	err := validator.ValidateURL(r.Url)
	if err != nil {
		return nil, err
	}

	// Generate an url that contains a shortened path.
	shortPath := service.generateRandomShortPath()
	service.linkStorage.Set(shortPath, r.Url)

	return &pb.ShortenResponse{ShortUrl: shortPath}, nil
}

// Resolve accepts a shortened URL as argument, and returns a corresponding original URL.
// It uses the LinkStorage to get an origin URL.
func (service *ShortenerService) Resolve(ctx context.Context, r *pb.ResolveRequest) (*pb.ResolveResponse, error) {
	return &pb.ResolveResponse{Url: service.linkStorage.Get(r.ShortUrl)}, nil
}

// Creates a new instance of the ShortenerService struct.
func NewShortenerService(linkStorage storage.LinkStorage) *ShortenerService {
	return &ShortenerService{linkStorage: linkStorage}
}

func (service *ShortenerService) generateRandomShortPath() string {
	rand.Seed(time.Now().UnixNano())

	pathBuff := make([]byte, shortPathLen)
	// We are generating short path until it won't be unique.
	for {
		for i := 0; i < shortPathLen; i++ {
			pathBuff[i] = alphabet[rand.Intn(alphabetLen-1)]
		}

		alreadyExistedValue := service.linkStorage.Get(string(pathBuff))
		if alreadyExistedValue == "" {
			break
		}
	}

	return string(pathBuff)
}
