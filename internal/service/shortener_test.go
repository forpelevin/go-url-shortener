package service

import (
	"context"
	"reflect"
	"testing"

	"gitlab.com/forpelevin/go-url-shortener/internal/pb"
	"gitlab.com/forpelevin/go-url-shortener/internal/storage"
)

func TestShortenerService(t *testing.T) {
	testCases := []struct {
		description         string
		inputURL            string
		wantShortenErr      bool
		wantResolveErr      bool
		expectedResolvedURL string
	}{
		{
			description:    "Empty URL should not be shortened.",
			inputURL:       "",
			wantShortenErr: true,
		},
		{
			description:    "Invalid formatted URL should not be shortened.",
			inputURL:       "host.com",
			wantShortenErr: true,
		},
		{
			description:         "Valid URL without path can be shortened and resolved successfully.",
			inputURL:            "http://host.com",
			expectedResolvedURL: "http://host.com",
		},
		{
			description:         "Valid URL with path can be shortened and resolved successfully.",
			inputURL:            "http://host.com/some/path",
			expectedResolvedURL: "http://host.com/some/path",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.description, func(t *testing.T) {
			shortener := NewShortenerService(storage.NewMemoryLinkStorage())
			req := &pb.ShortenRequest{Url: tc.inputURL}
			shortenResponse, err := shortener.Shorten(context.Background(), req)
			if (err != nil) != tc.wantShortenErr {
				t.Errorf("ShortenerService.Shorten() error = %v, wantErr %v", err, tc.wantShortenErr)
				return
			}

			if shortenResponse == nil {
				return
			}

			resReq := &pb.ResolveRequest{ShortUrl: shortenResponse.ShortUrl}
			resolveResponse, err := shortener.Resolve(context.Background(), resReq)
			if (err != nil) != tc.wantResolveErr {
				t.Errorf("ShortenerService.Resolve() error = %v, wantErr %v", err, tc.wantResolveErr)
				return
			}

			if resolveResponse == nil {
				return
			}

			if !reflect.DeepEqual(resolveResponse.Url, tc.expectedResolvedURL) {
				t.Errorf(
					"ShortenerService.Shorten() = %v, want %v",
					resolveResponse.Url,
					tc.expectedResolvedURL,
				)
			}
		})

	}
}
