package storage

import "sync"

// LinkStorage is the interface that wraps the methods for storing/fetching shortened links.
//
// Set sets given value to the given key.
// Get gets value by the given key.
type LinkStorage interface {
	Set(key string, value string)
	Get(key string) string
}

// MemoryLinkStorage is the struct for storing/fetching shortened links that uses a simple
// map under the hood.
type MemoryLinkStorage struct {
	links map[string]string
	mutex sync.RWMutex
}

// Set sets given value to the given key using its map.
func (mls *MemoryLinkStorage) Set(key string, value string) {
	mls.mutex.RLock()
	mls.links[key] = value
	mls.mutex.RUnlock()
}

// Get gets value by the given key using its map.
func (mls *MemoryLinkStorage) Get(key string) string {
	mls.mutex.Lock()
	defer mls.mutex.Unlock()
	val, ok := mls.links[key]
	if !ok {
		return ""
	}

	return val
}

// NewMemoryLinkStorage creates a new MemoryLinkStorage instance.
func NewMemoryLinkStorage() *MemoryLinkStorage {
	return &MemoryLinkStorage{links: make(map[string]string, 1024), mutex: sync.RWMutex{}}
}
