package server

import (
	"io/ioutil"
	"path"

	"github.com/caarlos0/env/v6"
	"gopkg.in/yaml.v2"
)

// Config is a struct that implements the server settings.
type Config struct {
	Host string `env:"SERVER_HOST"`
	Port string `env:"SERVER_PORT"`
}

// ReadYamlConfig reads a file with provided name in the config folder and return its content.
func ReadYamlConfig(filename string) ([]byte, error) {
	return ioutil.ReadFile(path.Join("config", filename))
}

// ReadServerConfig reads the server.yaml file from config directory, unmarshals it,
// and returns a filled config structure.
func ReadServerConfig() (*Config, error) {
	content, err := ReadYamlConfig("server.yaml")
	if err != nil {
		return nil, err
	}

	c := Config{}
	err = yaml.Unmarshal(content, &c)
	if err != nil {
		return nil, err
	}

	// Replace values by ENV if they were specified.
	if err := env.Parse(&c); err != nil {
		return nil, err
	}

	return &c, err
}
