package validator

import (
	"errors"
	"net/url"
)

var (
	errEmptyURL   = errors.New("URL cannot be empty")
	errInvalidURL = errors.New("URL has invalid format")
)

// ValidateURL validates the given URL string and return an error if it is invalid.
func ValidateURL(urlStr string) error {
	if urlStr == "" {
		return errEmptyURL
	}

	u, err := url.Parse(urlStr)
	if err != nil || u.Host == "" {
		return errInvalidURL
	}

	return nil
}
