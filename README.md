# Go URL shortener
The Golang module that can shorten and resolve URLs.

## Getting Started
1) Download the project from github to your host machine.
2) Go to the folder with project

## Prerequisites
For the successful using you must have:
```
go >= 1.12
```
## Running the tests
It's a good practice to run the tests before using the module to make sure everything is OK.
```
go test
```
## Sample of using
```go
shortener := NewDefaultShortenerService()
shortenedUrl := shortener.Shorten("http://mysite.ru") // will return something like: http://mysite.ru/Me3Jr
resolvedUrl := shortener.Resolve(shortenedUrl) // Will return your initial URL 
```
## License
This project is licensed under the MIT License.