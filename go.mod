module gitlab.com/forpelevin/go-url-shortener

go 1.12

require (
	github.com/caarlos0/env/v6 v6.0.0
	github.com/golang/protobuf v1.3.2
	github.com/joho/godotenv v1.3.0
	golang.org/x/crypto v0.0.0-20190820162420-60c769a6c586 // indirect
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7 // indirect
	golang.org/x/sys v0.0.0-20190813064441-fde4db37ae7a // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190822191935-b1e2c8edcefd // indirect
	google.golang.org/grpc v1.23.0
	gopkg.in/yaml.v2 v2.2.2
)
