package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"

	"github.com/joho/godotenv"
	"gitlab.com/forpelevin/go-url-shortener/internal/pb"
	"gitlab.com/forpelevin/go-url-shortener/internal/server"
	"gitlab.com/forpelevin/go-url-shortener/internal/service"
	"gitlab.com/forpelevin/go-url-shortener/internal/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	// Read the env file.
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}

	c, err := server.ReadServerConfig()
	if err != nil {
		log.Fatal(err)
	}

	// if we crash the go code, we get the file name and line number
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%s", c.Host, c.Port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()

	pb.RegisterShortenerServiceServer(s, service.NewShortenerService(storage.NewMemoryLinkStorage()))

	// Register reflection service on gRPC server.
	reflection.Register(s)

	go func() {
		fmt.Printf("Listening on: %s\n", c.Port)
		if err := s.Serve(lis); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	}()

	// Wait for Control C to exit
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	// Block until a signal is received
	<-ch
	s.Stop()
	err = lis.Close()
	if err != nil {
		log.Fatal(err)
	}
}
