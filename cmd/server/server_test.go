package main

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"testing"

	"gitlab.com/forpelevin/go-url-shortener/internal/pb"
	"google.golang.org/grpc"
)

const address = "localhost:50051"

func BenchmarkShortenerService(b *testing.B) {
	m := sync.Mutex{}
	b.RunParallel(func(pBench *testing.PB) {
		// Connect to the gRPC server via TCP.
		conn, err := grpc.Dial(fmt.Sprintf("%s", address), grpc.WithInsecure())
		if err != nil {
			m.Lock()
			b.Fatal(err)
		}
		defer conn.Close()

		// Create a gRPC client instance.
		client := pb.NewShortenerServiceClient(conn)
		for pBench.Next() {
			res, err := client.Shorten(context.Background(), &pb.ShortenRequest{Url: getRandomUrl()})
			if err != nil {
				m.Lock()
				b.Fatal(err)
			}

			_, err = client.Resolve(context.Background(), &pb.ResolveRequest{ShortUrl: res.ShortUrl})
			if err != nil {
				m.Lock()
				b.Fatal(err)
			}
		}
	})
}

func getRandomUrl() string {
	letters := "abcdefghijklmnopqrstuvwxyz"
	b := make([]byte, 5)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}

	return fmt.Sprintf("http://%s.ru", string(b))
}
